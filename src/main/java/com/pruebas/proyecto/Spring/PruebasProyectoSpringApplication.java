package com.pruebas.proyecto.Spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PruebasProyectoSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(PruebasProyectoSpringApplication.class, args);
	}

}
